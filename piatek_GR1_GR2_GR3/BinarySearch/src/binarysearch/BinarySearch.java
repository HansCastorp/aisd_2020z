package binarysearch;

public class BinarySearch {

    int search(double[] sortedNumbers, double toFind) {
        if (sortedNumbers == null) {
            throw new IllegalArgumentException("SortedNumbers cannot be null.");
        }

        int n = sortedNumbers.length;
        int left = 0;
        int right = n - 1;
        int mid;

        int resultId = -1;

        while (left <= right) {
            mid = (left + right) / 2;

            if (sortedNumbers[mid] == toFind) {
                resultId = mid;
                break;

            } else if (sortedNumbers[mid] > toFind) {
                right = mid - 1;

            } else {
                left = mid + 1;
            }
        }

        return resultId;

    }

}
