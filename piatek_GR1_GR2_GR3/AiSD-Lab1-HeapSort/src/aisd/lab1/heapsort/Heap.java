package aisd.lab1.heapsort;

import java.util.ArrayList;
import java.util.List;

public class Heap<T extends Comparable<T>> implements HeapInterface<T> {

    private final List<T> items;

    public Heap() {
        items = new ArrayList<>();
    }

    @Override
    public void put(T item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot put null into heap!");
        }

        items.add(item);

        int lastId = items.size() - 1;
        heapUp(lastId);
    }

    @Override
    public T pop() {
        int n = items.size();
        if (n == 0) {
            return null;
        }

        swapElems(0, n - 1);
        T resultVal = items.remove(n - 1);
        heapDown();

        return resultVal;
    }

    private void heapUp(int itemId) {
        int childId = itemId;
        int parentId = (childId - 1) / 2;

        while (parentId >= 0) {
            if (items.get(parentId).compareTo(items.get(childId)) < 0) {
                swapElems(parentId, childId);
                childId = parentId;
                parentId = (childId - 1) / 2;
            } else {
                break;
            }
        }
    }

    private void heapDown() {
        int n = items.size();
        int parentId = 0;
        int childId = 2 * parentId + 1;

        while (childId < n) {
            if (isRightChildBigger(n, childId)) {
                childId++;
            }
            if (items.get(childId).compareTo(items.get(parentId)) > 0) {
                swapElems(parentId, childId);
                parentId = childId;
                childId = 2 * parentId + 1;
            } else {
                break;
            }
        }
    }

    private boolean isRightChildBigger(int nItems, int leftChildId) {
        int rightChildId = leftChildId + 1;
        boolean rightIsBigger = false;

        if (rightChildId < nItems) {
            T leftChild = items.get(leftChildId);
            T rightChild = items.get(rightChildId);

            rightIsBigger = rightChild.compareTo(leftChild) > 0;
        }

        return rightIsBigger;
    }

    private void swapElems(int firstId, int secondId) {
        T firstVal = items.get(firstId);
        items.set(firstId, items.get(secondId));
        items.set(secondId, firstVal);
    }

}
