package aisd.lab1.heapsort;

import java.util.ArrayList;
import java.util.List;

public class Heap<T extends Comparable<T>> implements HeapInterface<T> {

    private List<T> items;

    public Heap() {
        items = new ArrayList<>();
    }

    @Override
    public void put(T item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot put null value!");
        }
        items.add(item);
        heapUp();
    }

    @Override
    public T pop() {
        if (items.isEmpty()) {
            return null;
        }

        int n = items.size();
        int firstItemId = 0;
        int lastItemId = n - 1;

        swapItems(firstItemId, lastItemId);
        T rootItems = items.remove(lastItemId);

        heapDown();

        return rootItems;
    }

    private void heapUp() {
        int n = items.size();
        int childId = n - 1;
        int parentId = (childId - 1) / 2;

        if (n > 1) {

            while (isChildBiggerThanParent(childId, parentId)) {
                swapItems(childId, parentId);
                if (parentId == 0) {
                    break;
                }
                childId = parentId;
                parentId = (childId - 1) / 2;
            }
        }
    }

    private void heapDown() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private boolean isChildBiggerThanParent(int childId, int parentId) {
        T childValue = items.get(childId);
        T parentValue = items.get(parentId);

        return childValue.compareTo(parentValue) > 0;
    }

    private void swapItems(int childId, int parentId) {
        T childValue = items.get(childId);
        T parentValue = items.get(parentId);

        items.set(childId, parentValue);
        items.set(parentId, childValue);
    }

}
