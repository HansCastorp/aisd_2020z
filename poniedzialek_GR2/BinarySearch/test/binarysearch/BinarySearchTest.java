package binarysearch;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class BinarySearchTest {

    private BinarySearch bSearch;

    @Before
    public void setUp() {
        bSearch = new BinarySearch();
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throwIllegalArgumentException_when_sortedNumbersIsNull() {
        // given
        double[] sortedNumbers = null;
        double numberToFind = 0.0;

        // when
        bSearch.search(sortedNumbers, numberToFind);

        // then
        assert false;
    }

    @Test
    public void should_reutrnNegativeId_when_numberToFindIsLowerThanMinOfSortedNumbers() {
        // given
        double[] sortedNumbers = {2, 3, 4, 5};
        double numberToFind = 1;

        // when
        int resultId = bSearch.search(sortedNumbers, numberToFind);
        int expectedId = -1;

        // then
        assertEquals(expectedId, resultId);
    }

}
