package aisd.lab2.sortcomparison;

public class InsertSort implements SortingAlgorithm {

    @Override
    public double[] sort(double[] unsortedVector) {
        if (unsortedVector == null) {
            throw new IllegalArgumentException("Unsorted vector cannot be null.");
        }

        double[] sortedNewVector = unsortedVector.clone();

        int n = sortedNewVector.length;

        for (int i = 1; i < n; i++) {
            int pos = i - 1;
            double val = sortedNewVector[i];

            while (pos >= 0 && sortedNewVector[pos] > val) {
                sortedNewVector[pos + 1] = sortedNewVector[pos];
                pos--;
            }
            sortedNewVector[pos + 1] = val;
        }

        return sortedNewVector;
    }

}
