package aisd.lab3.rbtree;

import aisd.lab3.map.MapInterface;
import org.junit.Test;
import static org.junit.Assert.*;

public class RbtMapTest {

    @Test
    public void should_getCorrectAnswerByCode() {
        // given
        MapInterface<String, String> passwords = new RbtMap<>();
        String code = "*****";
        String answer = "***";

        // when
        passwords.setValue(code, answer);
        String result = passwords.getValue(code);

        // then
        String expected = "***";
        assertEquals(expected, result);
    }

    @Test
    public void should_ovverideAnswerByCode() {
        //given
        MapInterface<String, String> passwords = new RbtMap<>();
        String code = "J23";
        String firstAnswer = "W Paryżu najlepsze kasztany są na placu Pigalle";
        String secondAnswer = "Zuzanna lubi je tylko jesienią";

        // when
        passwords.setValue(code, firstAnswer);
        passwords.setValue(code, secondAnswer);
        String result = passwords.getValue(code);

        // then
        String expected = secondAnswer;
        assertEquals(expected, result);
    }

}
